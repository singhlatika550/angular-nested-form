import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'nestedFormGroup';
  bioSection;
  constructor() {
    this.bioSection = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      age: new FormControl(''),
      stackDetails: new FormGroup({
        stack: new FormControl(''),
        experience: new FormControl('')
      }),
      address: new FormGroup({
        country: new FormControl(''),
        city: new FormControl('')
      })
    });
  }

  ngOnInit() {
  }

  callingFunction() {
    console.log('this.bioSection', this.bioSection.value);
  }
}
